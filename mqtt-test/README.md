# mqtt test example

Just testing mqtt with esp32. Connect to a known mqtt server, publish , subscribe and send mqtt messages  (from an external mqtt client) to the ESP32.

Remember:

a project needs a component.mk in main, even empty. So do `touch main/component.mk` before `make menuconfig`.

using mosquitto_sub to subscribe to topics: `mosquitto_sub -h mqtt-server.example.org -t public/topic/subtopic/#`

experiment 1: publish, subscribe and receive data from mqtt client.

output in esp32 monitor (`make monitor`)
```
I (15510) mqtt-test: Other event id:7
I (15860) MQTT_CLIENT: Sending MQTT CONNECT message, type: 1, id: 0000
I (16210) mqtt-test: MQTT_EVENT_CONNECTED
I (16210) mqtt-test: sent publish successful, msg_id=57858
I (16210) mqtt-test: sent subscribe successful, msg_id=30656
I (16210) mqtt-test: sent subscribe successful, msg_id=7605
I (16220) mqtt-test: sent unsubscribe successful, msg_id=54296
I (16230) mqtt-test: MQTT_EVENT_PUBLISHED, msg_id=57858
I (16240) mqtt-test: MQTT_EVENT_SUBSCRIBED, msg_id=30656
I (16240) mqtt-test: sent publish successful, msg_id=0
I (16250) mqtt-test: MQTT_EVENT_SUBSCRIBED, msg_id=7605
I (16250) mqtt-test: sent publish successful, msg_id=0
I (16250) mqtt-test: MQTT_EVENT_UNSUBSCRIBED, msg_id=7605
I (17440) mqtt-test: MQTT_EVENT_DATA
TOPIC=public/devel/ulo/subscribe/py2esp
DATA=esp32: data
I (17720) mqtt-test: MQTT_EVENT_DATA
TOPIC=public/devel/ulo/subscribe/py2esp
DATA=esp32: data
I (115340) mqtt-test: MQTT_EVENT_DATA
TOPIC=public/devel/ulo/subscribe/py2esp
DATA=from mqtt URL http://example.org/file.bin
```

output in mqtt client, subscribed to the relevant topics
```
esp32: data_3
esp32: data
esp32: data
from mqtt URL http://example.org/file.bin
```