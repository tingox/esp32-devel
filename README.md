# ESP32 development

More testing of ESP32. Development of more specific examples.

Project was created on Gitlab with 

`git push --set-upstream https://gitlab.com/tingox/esp32-devel.git master`

`git remote add origin https://gitlab.com/tingox/esp32-devel.git`

Then I changed visibility to public in Settings, General on the Gitlab project page.